<?php

require(__DIR__ . '/vendor/autoload.php');
Tester\Environment::setup();

define('HTTP_BIN', 'http://httpbin.org/response-headers?');

$httpRequest = new Nette\Http\Request(new Nette\Http\UrlScript);
$httpResponse = new Nette\Http\Response;

$response = new Adawolfa\Nette\Application\Responses\FileResponse(
	HTTP_BIN . http_build_query([
		'Content-Disposition' => 'attachment; filename=file.json',
	])
);

ob_start();
$response->send($httpRequest, $httpResponse);
ob_end_clean();

Tester\Assert::same('attachment; filename*=UTF-8\'\'file.json', $httpResponse->getHeader('Content-Disposition'));
Tester\Assert::same('application/json', $httpResponse->getHeader('Content-Type'));
Tester\Assert::same('101', $httpResponse->getHeader('Content-Length'));

$response = new Adawolfa\Nette\Application\Responses\FileResponse(
	HTTP_BIN . http_build_query([
		'Content-Disposition' => 'attachment; filename*=UTF-8\'\'file.json',
	])
);

ob_start();
$response->send($httpRequest, $httpResponse);
ob_end_clean();

Tester\Assert::same('attachment; filename*=UTF-8\'\'file.json', $httpResponse->getHeader('Content-Disposition'));
Tester\Assert::same('application/json', $httpResponse->getHeader('Content-Type'));
Tester\Assert::same('109', $httpResponse->getHeader('Content-Length'));

$response = new Adawolfa\Nette\Application\Responses\FileResponse(__DIR__ . '/.gitignore');
ob_start();
$response->send($httpRequest, $httpResponse);
ob_end_clean();

Tester\Assert::same('attachment; filename*=UTF-8\'\'.gitignore', $httpResponse->getHeader('Content-Disposition'));
Tester\Assert::same('application/octet-stream', $httpResponse->getHeader('Content-Type'));
Tester\Assert::same('21', $httpResponse->getHeader('Content-Length'));

$response = new Adawolfa\Nette\Application\Responses\FileResponse(__DIR__ . '/.gitignore', 'file.txt', 'text/plain', false);
ob_start();
$response->send($httpRequest, $httpResponse);
ob_end_clean();

Tester\Assert::same('inline; filename*=UTF-8\'\'file.txt', $httpResponse->getHeader('Content-Disposition'));
Tester\Assert::same('text/plain;charset=UTF-8', $httpResponse->getHeader('Content-Type'));
Tester\Assert::same('21', $httpResponse->getHeader('Content-Length'));